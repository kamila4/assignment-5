package Q2;

import java.util.ArrayList;
import java.util.function.Function;

public class Employee {
	int id;
	String name;
	double salary;
	
	Employee(int i, String n, double s){
		id = i;
		name = n;
		salary = s;
	}
	
	double incrSal() {
		return salary+= salary*0.1;
	}

	
	public static void main(String[] args) {
		
		ArrayList<Employee> em = new ArrayList<Employee>();
		
		em.add(new Employee(12,"Kamil",15000));
		em.add(new Employee(13,"Nishtha",19000));
		
		em.stream().map(p->p.incrSal()).forEach(System.out::println);
		
	}
}
