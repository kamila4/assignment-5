package Q3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Q3 {

	
	public static void main(String[] args) {
		
		ArrayList<String> pList1=new ArrayList<String>();
		ArrayList<String> pList2=new ArrayList<String>();
		ArrayList<String> pList3=new ArrayList<String>();
		
		
		pList1.add("HP Laptop");
		pList1.add("Acer Laptop");

		pList1.add("Samsung");
		

		pList2.add("HP phone");
		pList2.add("Acer");
		pList2.add("Samsung");
		

		pList3.add("camera");
		pList3.add("Acer");
		pList3.add("Samsung");
		
		
		ArrayList<String> finalList = (ArrayList<String>) Stream.concat(pList1.stream(), pList2.stream()).collect(Collectors.toList());
		finalList = (ArrayList<String>) Stream.of(pList3,finalList).flatMap(Collection::stream).collect(Collectors.toList());
		System.out.println(finalList);
	}
}
